import React from 'react'
import Link from 'next/link'
import Head from '../components/head'
import Nav from '../components/nav'
import Owl from './owl'

const Home = () => (
  <div>
    <Head title="Home" />

    <Nav />

    <div className="hero">
      <h1 className="title">Welcome to my first app NEXT!</h1>
    </div>

    <style jsx>{`
      .hero {
        width: 100%;
        color: #333;
      }
      .title {
        margin: 0;
        width: 100%;
        padding-top: 80px;
        line-height: 1.15;
        font-size: 48px;
      }
      .title {
        text-align: center;
      }
      
    `}</style>
  </div>
)

export default Home
