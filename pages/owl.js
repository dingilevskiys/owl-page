import React from 'react'
// import Link from 'next/link'
import Nav from "../components/nav";


const Owl = () => (

   <div>
      <Nav title="Home" />
      <div className='owl-section'>
         <div id='owl'>
            <div className='head'>
               <div className='ear'>
                  <div className='left'></div>
                  <div className='right'></div>
               </div>
               <div className='noze'></div>
               <div className='eye'>
                  <div className='center_eye'></div>
               </div>
               <div className='eye'>
                  <div className='center_eye'></div>
               </div>
            </div>
            <div className='body'>
               <div className='paws'></div>
               <div className='paws'></div>
            </div>
            <div className='puzo'>
               <div className='pugovici'></div>
               <div className='pugovici'></div>
               <div className='pugovici'></div>
               <div className='pugovici'></div>
               <div className='pugovici'></div>
               <div className='pugovici'></div>
            </div>
            <div className='legs'></div>
            <div className='legs'></div>
         </div>
         <style jsx>{`
html, body {
   min-height: 100vh;
   position: relative;
}
body {
   margin: 0;
}
.owl-section {
   width: 100%;
   height: 100%;
   text-align: center;
}
#owl {
   display: inline-block;
   height: 90%;
   position: relative;
}
#owl:hover {
   cursor: pointer;
}
#owl:hover .paws:nth-child(1) {
   transform: rotate(80deg);
   transform-origin: top center;
}
#owl:hover .paws:nth-child(2) {
   transform: rotate(-80deg);
   transform-origin: top center;
}
.head {
   margin-top: 10vh;
   width: 45vh;
   height: 32vh;
   background-color: #aad3ff;
   border-radius: 50%;
   position: relative;
}
.ear .left, .ear .right {
   position: absolute;
   z-index: -1;
   top: -2vh;
   left: -5vh;
   border-bottom: 8vh solid #aad3ff;
   border-left: 5vh solid transparent;
   border-right: 8vh solid transparent;
   height: 0;
   width: 6vh;
   transform: rotate(-125deg);
}
.ear .left:after, .ear .right:after {
   content: "";
   background-color: #ffffa9;
   position: absolute;
   top: 3vh;
   left: 4vh;
   width: 1vh;
   height: 2vh;
   transform: rotate(125deg);
   z-index: 10;
}
.ear .right {
   transform: scale(1, -1) rotate(55deg);
   left: 30vh;
}
.ear .right:after {
   width: 1.25vh;
}
.noze {
   position: absolute;
   height: 10vh;
   width: 10vh;
   background-color: #ffffa9;
   top: 20vh;
   left: 17.5vh;
   transform: scale(1, 0.65) rotate(-125deg) skew(25deg);
   z-index: 9;
}
.eye {
   position: absolute;
   z-index: 5;
   content: "";
   top: 6vh;
   left: 4vh;
   width: 20vh;
   height: 20vh;
   background-color: #000;
   border-radius: 50%;
}
.eye:before {
   position: absolute;
   z-index: 6;
   top: 0.75vh;
   left: 0.75vh;
   width: 18.5vh;
   height: 18.5vh;
   background-color: #fff;
   border-radius: 50%;
   content: "";
}
.eye:after {
   position: absolute;
   z-index: 7;
   content: "";
   top: 5vh;
   left: 5vh;
   width: 10vh;
   height: 10vh;
   background-color: #000;
   border-radius: 50%;
}
.eye .center_eye {
   position: absolute;
   height: 3vh;
   width: 2.5vh;
   background-color: #fff;
   border-radius: 50%;
   top: 8vh;
   left: 9.25vh;
   z-index: 8;
}
.eye:last-child {
   position: absolute;
   left: 21vh;
}
.body {
   margin-top: 10vh;
   position: absolute;
   top: 10vh;
   left: 2.5vh;
   width: 40vh;
   height: 55vh;
   background-color: #7bb7f5;
   border-radius: 50%;
   z-index: -1;
}
.paws {
   position: absolute;
   height: 37.5vh;
   width: 20.5vh;
   border-radius: 50%;
   background-color: #aad3ff;
   left: -3.5vh;
   top: 14.5vh;
   transform: rotate(20deg);
   transition: all 1s ease-out;
}
.paws[class~=paws]:last-child {
   transform: rotate(-20deg);
   left: 23vh;
}
.puzo {
   position: relative;
   top: -15.5vh;
   left: 5vh;
   height: 45vh;
   width: 35vh;
   border-radius: 50%;
   z-index: -1;
   background-color: #499bed;
}
.pugovici {
   position: absolute;
   content: "";
   width: 4vh;
   height: 4vh;
   border-radius: 50%;
   background-color: #ffffa9;
   z-index: 10;
}
.pugovici:nth-child(1) {
   top: 23vh;
   left: 3vh;
}
.pugovici:nth-child(2) {
   top: 22vh;
   left: 10vh;
}
.pugovici:nth-child(3) {
   top: 22vh;
   left: 20vh;
}
.pugovici:nth-child(4) {
   top: 23vh;
   left: 27vh;
}
.pugovici:nth-child(5) {
   top: 30vh;
   left: 9vh;
   }
.pugovici:nth-child(6) {
   top: 30vh;
   left: 21vh;
}

.legs {
   position: relative;
   top: -15vh;
   left: 13vh;
   height: 6vh;
   width: 2.5vh;
   border-radius: 50%;
   background-color: #512e0e;
}
.legs:before {
   content: "";
   position: absolute;
   top: -0.5vh;
   left: -1.5vh;
   height: 6vh;
   width: 2.5vh;
   border-radius: 50%;
   background-color: #512e0e;
}
.legs:after {
   content: "";
   position: absolute;
   top: 0.5vh;
   left: 1.5vh;
   height: 6vh;
   width: 2.5vh;
   border-radius: 50%;
   background-color: #512e0e;
}
.legs:last-child {
   transform: scale(1, -1);
   top: -20.5vh;
   left: 28vh;
}
`}</style>
      </div>
   </div>
)

export default Owl
