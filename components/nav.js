import React from 'react'
import Link from 'next/link'


const Nav = () => (
  <nav>
    <ul>
      <li>
        <Link prefetch href="/">
          <a>Home</a>
        </Link>

      </li>
      <li><Link href="/owl">
        <a>Owl</a>
      </Link></li>

    </ul>

    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
      }
      nav {
        text-align: center;
      }
      ul{
        list-style: none;
      }
      nav > ul {
        padding: 4px 16px;
      }
      li {
        float:left;
        padding: 6px 8px;
        margin-left: 10px; 
      }
      a {
        color: #067df7;
        text-decoration: none;
        font-size: 13px;
      }

    `}</style>
  </nav>
)

export default Nav
